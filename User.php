<?php

class User{
protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

/**
* User constructor.
* @param $name
*/
public function __construct($name)
{
$this->name = $name;
}

}